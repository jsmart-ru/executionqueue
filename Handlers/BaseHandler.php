<?php

class BaseHandler implements ExecutionQueueInterface
{
    public function __construct()
    {
        if (empty($_SESSION)) {
            session_start();
        }
    }

    public function put($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function get($key) {
        return $_SESSION[$key] ?? false;
    }
}