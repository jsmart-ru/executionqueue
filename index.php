<?php

spl_autoload_register(function ($class_name) {

    $file = realpath(dirname(__FILE__)) . '/' . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';

    if (file_exists($file)) {
        include $file;
    }
});

include 'ExecutionQueueInterface.php';
include 'Handlers/BaseHandler.php';
include 'ExecutionQueue.php';

$executionQueue = new ExecutionQueue();

$executionQueue->add(catalogsImport::class, ['one', 'two', 'three', 'four', 'five'], 3);

$executionQueue->add(formsImport::class, ['one 1', 'two 2']);

$executionQueue->add(function ($data) {
    echo 'function:<br>'; print_r($data); echo '<hr>';
}, ['four', 'five'], 2);

$a = $executionQueue->run();

echo '<hr>'; print_r($a);

// Тестовые обработчики //
class catalogsImport
{
    public function __invoke($data)
    {
        echo 'catalogsImport:<br>'; print_r($data); echo '<hr>';
    }
}

class formsImport
{
    public function __invoke($data)
    {
        echo 'formsImport:<br>'; print_r($data); echo '<hr>';
    }
}